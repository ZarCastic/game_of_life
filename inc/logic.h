#pragma once

#include <QObject>
#include <QTimer>
#include <vector>

namespace GameOfLife {

class Logic : public QObject {
    Q_OBJECT

    using BlobState = bool;

public:
    Logic(uint8_t rows, uint8_t columns, uint8_t speed, bool paused, QObject *parent = nullptr);

    void onWidthChanged(uint8_t columns);
    void onHeightChanged(uint8_t rows);
    void onAddBlob();
    void onRemoveBlob();
    void onPositionClicked(uint8_t row, uint8_t column);
    void onPauseClicked();
    void onSpeedChanged(uint8_t speed_index);

signals:
    void newBlob(uint8_t row, uint8_t column);
    void removeBlob(uint8_t row, uint8_t column);
    void newSize(uint8_t rows, uint8_t columns);
    void pauseState(bool paused);
    void newGrid(const std::vector<std::pair<uint8_t, uint8_t>> &blobs);

private:
    void    _makePossibleBlobs();
    void    _togglePosition(uint8_t row, uint8_t column);
    void    _onStepTimeout();
    uint8_t _live_neighbors(const std::pair<uint8_t, uint8_t> &pos);
    void    _printBlobs();

    std::vector<std::vector<BlobState>>      _grid;
    std::vector<std::pair<uint8_t, uint8_t>> _blobs;
    size_t                                   _num_blobs = 0;
    uint8_t                                  _columns;
    uint8_t                                  _rows;
    bool                                     _paused;
    QTimer                                   _step_timer;
};

} // namespace GameOfLife
