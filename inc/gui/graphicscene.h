#pragma once

#include <QGraphicsScene>

namespace GameOfLife::Gui {

class GraphicScene : public QGraphicsScene {

    Q_OBJECT
public:
    GraphicScene();

signals:
    void mouseReleased(size_t y, size_t x);
    void mousePressed(size_t y, size_t x);

private:
    void mousePressEvent(QGraphicsSceneMouseEvent *mouseEvent) override;
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *mouseEvent) override;
};

} // namespace GameOfLife::Gui
