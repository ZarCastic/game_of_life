#pragma once

#include "gui/field.h"
#include <QMainWindow>

namespace GameOfLife::Gui {

class MainWindow : public QMainWindow {
    Q_OBJECT
public:
    explicit MainWindow(uint8_t rows, uint8_t columns, uint8_t speed, bool paused, QWidget *parent = nullptr);
    virtual ~MainWindow() = default;

    void addBlob(uint8_t row, uint8_t column);
    void removeBlob(uint8_t row, uint8_t column);

signals:
    void addBlobClicked();
    void removeBlobClicked();
    void resizedWidth(uint8_t columns);
    void resizedHeight(uint8_t rows);
    void positionClicked(uint8_t row, uint8_t column);
    void onPauseClicked();
    void onNewSize(uint8_t rows, uint8_t columns);
    void pauseState(bool paused);
    void speedChanged(uint8_t speed_index);
    void onNewGrid(const std::vector<std::pair<uint8_t, uint8_t>> &blobs);

private:
    Field *_field;
};

} // namespace GameOfLife::Gui
