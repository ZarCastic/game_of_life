#pragma once

#include "gui/graphicscene.h"
#include <QGraphicsView>
#include <QRect>
#include <QWidget>
#include <vector>

namespace GameOfLife::Gui {

class Field : public QWidget {
    Q_OBJECT
public:
    explicit Field(uint8_t rows, uint8_t columns, QWidget *parent = nullptr);
    virtual ~Field() = default;

    void        setColumns(uint8_t columns);
    void        setRows(uint8_t rows);
    void        setSize(uint8_t columns, uint8_t rows);
    void        onNewGrid(const std::vector<std::pair<uint8_t, uint8_t>> &blobs);
    const QRect at(uint8_t row, uint8_t column);

    void addDot(uint8_t row, uint8_t column);
    void removeDot(uint8_t row, uint8_t column);

signals:
    void positionClicked(uint8_t row, uint8_t column);

private:
    void _addGrid();
    void _onMouseReleased(size_t y, size_t x);

    GraphicScene        _draw_scene;
    QGraphicsView       _draw_view;
    std::vector<size_t> _xs;
    std::vector<size_t> _ys;
    size_t              _dot_width;
    size_t              _dot_height;
};

} // namespace GameOfLife::Gui
