#pragma once

#include <QPushButton>
#include <QSpinBox>
#include <QWidget>

namespace GameOfLife::Gui {

class Hud : public QWidget {
    Q_OBJECT
public:
    explicit Hud(uint8_t rows, uint8_t columns, uint8_t speed, bool paused, QWidget *parent = nullptr);
    virtual ~Hud() = default;

    void onSizeChanged(uint8_t rows, uint8_t columns);
    void onPauseStateChanged(bool paused);

signals:
    void addBlobClicked();
    void removeBlobClicked();
    void resizedWidth(uint8_t columns);
    void resizedHeight(uint8_t rows);
    void pauseState(bool paused);
    void pauseClicked();
    void speedChanged(uint8_t speed_index);

private:
    QSpinBox *   _col_spin;
    QSpinBox *   _row_spin;
    QPushButton *_pause_button;
};

} // namespace GameOfLife::Gui
