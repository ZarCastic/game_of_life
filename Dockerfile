FROM ubuntu:latest

RUN apt update 
RUN apt install build-essential cmake git qt5-default wget -y
RUN wget https://github.com/google/googletest/archive/release-1.10.0.tar.gz ; \
    tar xf release-1.10.0.tar.gz ; rm release-1.10.0.tar.gz; \
    cd googletest-release-1.10.0 ;\
    cmake -DBUILD_SHARED_LIBS=ON .  ;\
    make; make install; \
    cd / ; \
    rm -rf googletest-release-1.10.0
