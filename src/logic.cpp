#include "logic.h"
#include <QDebug>
#include <algorithm>
#include <chrono>
#include <random>
#include <vector>

namespace GameOfLife {

using namespace std::chrono_literals;
static std::chrono::milliseconds speed_steps[] = {
    50ms, 100ms, 250ms, 500ms, 750ms, 1000ms, 1250ms, 1500ms, 2000ms, 3000ms};

Logic::Logic(uint8_t rows, uint8_t columns, uint8_t speed, bool paused, QObject *parent) :
  QObject(parent), _grid(columns, std::vector<BlobState>(rows, false)), _columns(columns), _rows(rows), _paused(paused) {

    connect(&_step_timer, &QTimer::timeout, this, &Logic::_onStepTimeout);
    _step_timer.setSingleShot(true);
    _step_timer.setInterval(speed_steps[speed]);
    if(!_paused) {
        _step_timer.start();
    }
}

void Logic::onWidthChanged(uint8_t columns) {
    std::for_each(_grid.begin(), _grid.end(), [columns](auto &row) { row.resize(columns, false); });
    _columns = columns;

    auto last_el = std::remove_if(_blobs.begin(), _blobs.end(),
        [columns](auto blob) {
            return blob.second >= columns;
        });
    _blobs.erase(last_el, _blobs.end());

    emit newSize(_rows, _columns);
    emit newGrid(_blobs);
}

void Logic::onHeightChanged(uint8_t rows) {
    _grid.resize(rows, std::vector<BlobState>(_columns, false));
    _rows = rows;

    std::remove_if(_blobs.begin(), _blobs.end(), [rows](auto blob) { return blob.first >= rows; });

    emit newSize(_rows, _columns);
    emit newGrid(_blobs);
}

void Logic::onAddBlob() {
    static std::default_random_engine     generator;
    std::uniform_int_distribution<size_t> row_dist(0, _rows);
    std::uniform_int_distribution<size_t> col_dist(0, _columns);

    auto row = row_dist(generator);
    auto col = col_dist(generator);
    while(_grid[row][col]) {
        row = row_dist(generator);
        col = col_dist(generator);
    }
    _grid[row][col] = false;
    _blobs.push_back({row, col});
    _num_blobs++;
    emit newBlob(row, col);
}

void Logic::onRemoveBlob() {
    if(_num_blobs == 0) {
        return;
    }

    static std::default_random_engine     generator;
    std::uniform_int_distribution<size_t> row_dist(0, _blobs.size() - 1);

    const auto blob_idx = row_dist(generator);
    const auto blob     = _blobs[blob_idx];

    _blobs.erase(_blobs.begin() + blob_idx);
    _grid[blob.first][blob.second] = false;
    _num_blobs--;
    emit removeBlob(blob.first, blob.second);
}

void Logic::onPositionClicked(uint8_t row, uint8_t column) {
    _togglePosition(row, column);
}

void Logic::_togglePosition(uint8_t row, uint8_t column) {
    bool is_living     = _grid[row][column];
    _grid[row][column] = !is_living;

    if(is_living) {
        _blobs.erase(std::find(_blobs.begin(), _blobs.end(), std::pair<uint8_t, uint8_t>{row, column}));
        _num_blobs--;
        emit removeBlob(row, column);
    } else {
        emit newBlob(row, column);
        _num_blobs++;
        _blobs.push_back({row, column});
    }
}

void Logic::onPauseClicked() {
    _paused = !_paused;

    if(_paused) {
        _step_timer.stop();
    } else {
        _step_timer.start();
    }

    emit pauseState(_paused);
}

void Logic::onSpeedChanged(uint8_t speed_index) {
    _step_timer.setInterval(speed_steps[speed_index]);
    if(_paused) {
        _step_timer.stop();
        _step_timer.start();
    }
}

void Logic::_onStepTimeout() {
    std::vector<std::vector<BlobState>>      next_grid(_columns, std::vector<BlobState>(_rows, false));
    std::vector<std::pair<uint8_t, uint8_t>> next_blobs;

    for(auto col = 0; col < _columns; col++) {
        for(auto row = 0; row < _rows; row++) {
            const auto current_state  = _grid[col][row];
            const auto live_neighbors = _live_neighbors({col, row});
            if((current_state && live_neighbors >= 2 && live_neighbors <= 3) || (!current_state && live_neighbors == 3)) {
                next_grid[col][row] = true;
                next_blobs.push_back({col, row});
            }
        }
    }

    _step_timer.start();
    _blobs = std::move(next_blobs);
    _grid  = std::move(next_grid);

    emit newSize(_rows, _columns);
    emit newGrid(_blobs);
}

void Logic::_printBlobs() {
    qDebug() << "---------------------------------------";
    for(auto blob : _blobs) {
        qDebug() << blob;
    }
}

uint8_t Logic::_live_neighbors(const std::pair<uint8_t, uint8_t> &pos) {
    uint8_t retval = 0;

    for(auto col_ofs = -1; col_ofs < 2; ++col_ofs) {
        if(pos.first + col_ofs < 0 || pos.first + col_ofs >= _columns) {
            continue;
        }
        for(auto row_ofs = -1; row_ofs < 2; ++row_ofs) {
            if(pos.second + row_ofs < 0 || pos.second + row_ofs >= _rows || (row_ofs == 0 && col_ofs == 0)) {
                continue;
            }
            if(_grid[pos.first + col_ofs][pos.second + row_ofs]) {
                retval++;
            }
        }
    }

    return retval;
}

} // namespace GameOfLife
