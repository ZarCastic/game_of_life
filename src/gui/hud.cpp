#include "gui/hud.h"
#include <QHBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QSlider>
#include <QSpinBox>
#include <QVBoxLayout>

namespace GameOfLife::Gui {

Hud::Hud(uint8_t rows, uint8_t columns, uint8_t speed, bool paused, QWidget *parent) :
  QWidget(parent), _row_spin(new QSpinBox()), _col_spin(new QSpinBox()),
  _pause_button(new QPushButton((paused ? "Start" : "Pause"))) {
    setLayout(new QVBoxLayout());

    auto row_widget = new QWidget();
    auto row_layout = new QHBoxLayout();
    row_layout->addWidget(new QLabel("Number of rows: "));
    _row_spin->setRange(10, 100);
    _row_spin->setValue(rows);
    row_layout->addWidget(_row_spin);
    row_widget->setLayout(row_layout);
    layout()->addWidget(row_widget);

    auto col_widget = new QWidget();
    auto col_layout = new QHBoxLayout();
    col_layout->addWidget(new QLabel("Number of columns: "));
    _col_spin->setRange(10, 100);
    _col_spin->setValue(columns);
    col_layout->addWidget(_col_spin);
    col_widget->setLayout(col_layout);
    layout()->addWidget(col_widget);

    auto blob_widget     = new QWidget();
    auto blob_layout     = new QVBoxLayout();
    auto add_blob_button = new QPushButton("Add Blob");
    blob_layout->addWidget(add_blob_button);
    auto remove_blob_button = new QPushButton("Remove Blob");
    blob_layout->addWidget(remove_blob_button);
    blob_widget->setLayout(blob_layout);
    layout()->addWidget(blob_widget);

    auto speed_slider = new QSlider(Qt::Horizontal);
    speed_slider->setMinimum(0);
    speed_slider->setMaximum(9);
    speed_slider->setValue(speed);
    speed_slider->setTickInterval(1);
    speed_slider->setTickPosition(QSlider::TicksBelow);

    layout()->addWidget(_pause_button);
    layout()->addWidget(speed_slider);

    connect(_pause_button, &QPushButton::clicked, this, &Hud::pauseClicked);
    dynamic_cast<QVBoxLayout *>(layout())->addStretch();

    connect(add_blob_button, &QPushButton::clicked, this, &Hud::addBlobClicked);
    connect(remove_blob_button, &QPushButton::clicked, this, &Hud::removeBlobClicked);
    connect(_row_spin, QOverload<int>::of(&QSpinBox::valueChanged), this, &Hud::resizedHeight);
    connect(_col_spin, QOverload<int>::of(&QSpinBox::valueChanged), this, &Hud::resizedWidth);
    connect(speed_slider, &QSlider::valueChanged, this, &Hud::speedChanged);
}

void Hud::onSizeChanged(uint8_t rows, uint8_t columns) {
    _row_spin->setValue(rows);
    _col_spin->setValue(columns);
}

void Hud::onPauseStateChanged(bool paused) {
    _pause_button->setText((paused ? "Start" : "Pause"));
}

} // namespace GameOfLife::Gui
