#include "gui/gui.h"
#include "gui/field.h"
#include "gui/hud.h"
#include <QHBoxLayout>

namespace GameOfLife::Gui {

MainWindow::MainWindow(uint8_t rows, uint8_t columns, uint8_t speed, bool paused, QWidget *parent) :
  QMainWindow(parent), _field(new Field(rows, columns)) {
    auto main_layout = new QHBoxLayout();

    main_layout->addWidget(_field);
    auto hud = new Hud(rows, columns, speed, paused);
    main_layout->addWidget(hud);

    setCentralWidget(new QWidget());
    centralWidget()->setLayout(main_layout);

    connect(hud, &Hud::addBlobClicked, this, &MainWindow::addBlobClicked);
    connect(hud, &Hud::removeBlobClicked, this, &MainWindow::removeBlobClicked);
    connect(hud, &Hud::resizedWidth, this, &MainWindow::resizedWidth);
    connect(hud, &Hud::resizedHeight, this, &MainWindow::resizedHeight);
    connect(hud, &Hud::pauseClicked, this, &MainWindow::onPauseClicked);
    connect(hud, &Hud::speedChanged, this, &MainWindow::speedChanged);
    connect(this, &MainWindow::pauseState, hud, &Hud::onPauseStateChanged);
    connect(_field, &Field::positionClicked, this, &MainWindow::positionClicked);
    connect(this, &MainWindow::onNewSize, _field, &Field::setSize);
    connect(this, &MainWindow::onNewGrid, _field, &Field::onNewGrid);
    //    connect(this, &MainWindow::onNewSize, hud, &Hud::onSizeChanged);
}

void MainWindow::addBlob(uint8_t row, uint8_t column) {
    _field->addDot(row, column);
}

void MainWindow::removeBlob(uint8_t row, uint8_t column) {
    _field->removeDot(row, column);
}

} // namespace GameOfLife::Gui
