#include "gui/graphicscene.h"
#include <QDebug>
#include <QGraphicsSceneMouseEvent>

namespace GameOfLife::Gui {

GraphicScene::GraphicScene() {
}

void GameOfLife::Gui::GraphicScene::mousePressEvent(QGraphicsSceneMouseEvent *mouseEvent) {
    auto pos = mouseEvent->scenePos();
    emit mousePressed(pos.y(), pos.x());
}

void GameOfLife::Gui::GraphicScene::mouseReleaseEvent(QGraphicsSceneMouseEvent *mouseEvent) {
    auto pos = mouseEvent->scenePos();
    emit mouseReleased(pos.y(), pos.x());
}

} // namespace GameOfLife::Gui
