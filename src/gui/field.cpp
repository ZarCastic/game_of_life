#include "gui/field.h"
#include <QDebug>
#include <QGraphicsEllipseItem>
#include <QGraphicsView>
#include <QLabel>
#include <QVBoxLayout>
#include <algorithm>
#include <iostream>
#include <vector>

namespace GameOfLife::Gui {

Field::Field(uint8_t rows, uint8_t columns, QWidget *parent) :
  QWidget(parent), _draw_scene(), _draw_view(&_draw_scene) {
    setLayout(new QVBoxLayout());
    layout()->addWidget(new QLabel("Field"));

    layout()->addWidget(&_draw_view);
    _draw_scene.setBackgroundBrush(Qt::white);

    _draw_view.setFixedSize(700, 700);
    setSize(columns, rows);

    connect(&_draw_scene, &GraphicScene::mouseReleased, this, &Field::_onMouseReleased);
}

void Field::setColumns(uint8_t columns) {
    _dot_width = _draw_view.width() / columns;

    _xs.clear();
    _xs      = std::vector<size_t>(columns);
    size_t x = 0;
    std::generate(_xs.begin(), _xs.end(), [&x, this]() {x += _dot_width; return x; });

    _addGrid();
}

void Field::setRows(uint8_t rows) {
    _dot_height = _draw_view.height() / rows;

    _ys.clear();
    _ys      = std::vector<size_t>(rows);
    size_t y = 0;
    std::generate(_ys.begin(), _ys.end(), [&y, this]() { y += _dot_height; return y; });

    _addGrid();
}

void Field::setSize(uint8_t rows, uint8_t columns) {
    _dot_height = _draw_view.height() / rows;
    _dot_width  = _draw_view.width() / columns;

    _ys.clear();
    _ys      = std::vector<size_t>(rows);
    size_t y = 0;
    std::generate(_ys.begin(), _ys.end(), [&y, this]() {y += _dot_height; return y; });

    _xs.clear();
    _xs      = std::vector<size_t>(columns);
    size_t x = 0;
    std::generate(_xs.begin(), _xs.end(), [&x, this]() { x += _dot_width; return x; });

    _addGrid();
}

void Field::onNewGrid(const std::vector<std::pair<uint8_t, uint8_t>> &blobs) {
    // assumes scene had been redrawn before
    for(const auto &blob : blobs) {
        addDot(blob.first, blob.second);
    }
}

const QRect Field::at(uint8_t row, uint8_t column) {
    const auto retval = QRect(column * _dot_width, row * _dot_height, _dot_width, _dot_height);
    return retval;
}

void Field::addDot(uint8_t row, uint8_t column) {
    const auto rect = at(row, column);
    _draw_scene.addEllipse(rect, QPen(Qt::blue), QBrush(Qt::blue));
}

void Field::removeDot(uint8_t row, uint8_t column) {
    const auto pixel_row    = row * _dot_height + 0.5 * _dot_height;
    const auto pixel_column = column * _dot_width + 0.5 * _dot_width;

    const auto item = _draw_scene.itemAt(pixel_column, pixel_row, QTransform());
    if(!dynamic_cast<QGraphicsEllipseItem *>(item)) {
        qDebug() << "cannot cast ellipse item. Not an ellipse";
    }
    _draw_scene.removeItem(item);
}

void Field::_addGrid() {
    const auto size   = _draw_view.size();
    const auto width  = size.width();
    const auto height = size.height();
    _draw_scene.clear();

    for(const auto x : _xs) {
        _draw_scene.addLine(x, 0, x, height);
    }
    for(const auto y : _ys) {
        _draw_scene.addLine(0, y, width, y);
    }
}

void Field::_onMouseReleased(size_t y, size_t x) {
    auto row    = y / _dot_height;
    auto column = x / _dot_width;

    emit positionClicked(row, column);
}

} // namespace GameOfLife::Gui
