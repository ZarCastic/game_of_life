#include "gui/gui.h"
#include "logic.h"
#include <QApplication>
#include <QObject>
#include <iostream>

int main(int argc, char **argv) {

    QApplication                app(argc, argv);
    GameOfLife::Gui::MainWindow gui(70, 70, 5, true);
    GameOfLife::Logic           logic(70, 70, 5, true);

    gui.show();

    QObject::connect(&gui, &GameOfLife::Gui::MainWindow::resizedWidth, &logic, &GameOfLife::Logic::onWidthChanged);
    QObject::connect(&gui, &GameOfLife::Gui::MainWindow::resizedHeight, &logic, &GameOfLife::Logic::onHeightChanged);
    QObject::connect(&gui, &GameOfLife::Gui::MainWindow::addBlobClicked, &logic, &GameOfLife::Logic::onAddBlob);
    QObject::connect(&gui, &GameOfLife::Gui::MainWindow::removeBlobClicked, &logic, &GameOfLife::Logic::onRemoveBlob);
    QObject::connect(&gui, &GameOfLife::Gui::MainWindow::positionClicked, &logic, &GameOfLife::Logic::onPositionClicked);
    QObject::connect(&gui, &GameOfLife::Gui::MainWindow::onPauseClicked, &logic, &GameOfLife::Logic::onPauseClicked);
    QObject::connect(&gui, &GameOfLife::Gui::MainWindow::speedChanged, &logic, &GameOfLife::Logic::onSpeedChanged);

    QObject::connect(&logic, &GameOfLife::Logic::newBlob, &gui, &GameOfLife::Gui::MainWindow::addBlob);
    QObject::connect(&logic, &GameOfLife::Logic::removeBlob, &gui, &GameOfLife::Gui::MainWindow::removeBlob);
    QObject::connect(&logic, &GameOfLife::Logic::newSize, &gui, &GameOfLife::Gui::MainWindow::onNewSize);
    QObject::connect(&logic, &GameOfLife::Logic::pauseState, &gui, &GameOfLife::Gui::MainWindow::pauseState);
    QObject::connect(&logic, &GameOfLife::Logic::newGrid, &gui, &GameOfLife::Gui::MainWindow::onNewGrid);

    return QApplication::exec();
}
